<div align="center">
<img width="25%" height = "225px" src="https://gifimage.net/wp-content/uploads/2017/10/discord-profile-picture-gif-3.gif" alt="cover" />
</div>

<h1> Hello Fellow < Developers/ >! <img src = "https://raw.githubusercontent.com/MartinHeinz/MartinHeinz/master/wave.gif" width = 50px> </h1>
<div size='20px'> Hi! My name is Adarsh. Thank You for taking the time to view my GitHub Profile :smile: 
</div>

<h2> About Me <img src = "https://media0.giphy.com/media/KDDpcKigbfFpnejZs6/giphy.gif?cid=ecf05e47oy6f4zjs8g1qoiystc56cu7r9tb8a1fe76e05oty&rid=giphy.gif" width = 100px></h2>

<img width="55%" align="right" alt="Github" src="https://raw.githubusercontent.com/onimur/.github/master/.resources/git-header.svg" />


- 🔭 I’m currently working on Something

- 🌱 I’m currently learning Python 

- 👯 I’m looking to collaborate on Projects 

- 💬 Talk to me about Python, Open Source 

<h2> Skills <img src = "https://media2.giphy.com/media/QssGEmpkyEOhBCb7e1/giphy.gif?cid=ecf05e47a0n3gi1bfqntqmob8g9aid1oyj2wr3ds3mg700bl&rid=giphy.gif" width = 32px> </h2>
<a href= https://github.com/theCode-Breaker?tab=repositories&q=&type=&language=python&sort= > <img width ='32px' src ='https://raw.githubusercontent.com/rahulbanerjee26/githubAboutMeGenerator/main/icons/python.svg'> </a>


<h2> Connect with me <img src='https://raw.githubusercontent.com/ShahriarShafin/ShahriarShafin/main/Assets/handshake.gif' width="100px"> </h2>
<a href = 'https://www.github.com/theCode-Breaker'> <img width = '32px' align= 'center' src="https://raw.githubusercontent.com/rahulbanerjee26/githubAboutMeGenerator/main/icons/github.svg"/></a>
<a href = 'https://gitlab.com/theCode-Breaker'> <img width = '42px' align= 'center' 
src="https://about.gitlab.com/images/press/logo/svg/gitlab-icon-rgb.svg"/></a>

<h2> Some Programming Humor for you <img align ='center' src='https://media2.giphy.com/media/UQDSBzfyiBKvgFcSTw/giphy.gif?cid=ecf05e47p3cd513axbek3f56ti3jzizq8hincw20jauyyfyw&rid=giphy.gif' width = '32px'></h2>

![Jokes Card](https://readme-jokes.vercel.app/api?theme=tokyonight)
